package handlers

import (
	"fmt"
	"io/ioutil"
	"os/exec"

	"github.com/gofiber/fiber/v2"
)

type Body struct {
	Code string `json:"code"`
	Input string `json:"input"`
}

func Run(c *fiber.Ctx) error {
	var body Body
	if err := c.BodyParser(&body); err != nil {
		return c.Status(503).SendString(err.Error())
	}

	err := writeFile("main.go", body.Code)
	if err != nil {
		return c.Status(503).SendString(err.Error())
	}
	err = writeFile("input.txt", body.Input)
	if err != nil {
		return c.Status(503).SendString(err.Error())
	}

	result, err := judge()
	if err != nil {
		return c.Status(500).SendString("プログラム実行中にエラーが発生しました")
	}
	return c.Status(200).JSON(result)
}

func writeFile(filepath string, str string) error {
	name := fmt.Sprintf("data/%s", filepath)

	err := ioutil.WriteFile(name, []byte(str), 0644)
	if err != nil {
		return err
	}
	return nil
}

func judge() (string, error) {
	out, err := exec.Command("sh", "-c", "cd data && sh judge.sh").Output()

	if err != nil {
		return "", err
	}

	return string(out), nil
}

// func judge() (string, error) {
// 	out, err := exec.Command("docker", "run", "--rm", "fullstack_judge", "sh", "-c", "cd tmp/exec && bash judge.sh").Output()

// 	if err != nil {
// 		return "", err
// 	}

// 	return string(out), nil
// }

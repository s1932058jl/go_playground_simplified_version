package main

import (
	"online-judge-lab/handlers"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	app := fiber.New()
	app.Use(cors.New())

	app.Post("/api/v1/judge", handlers.Run)

	app.Listen(":8080")
}